var fmtNumber = function(n) {
    if (!Number.toLocaleString) {
        Number.prototype.toLocaleString = function(n) {
            return this
        }
    }

    return new Number(n).toLocaleString("en-US")
}

var TJvsTheWorld = function() {
    this.spoilers = {}
    this.seenActions = {}

    this.setupWebSockets()
    this.setupOnEvents()
}

TJvsTheWorld.Config = {
    SocketConnectRetries: 5,
    SocketConnectRetryDelayMS: 2500,
}

TJvsTheWorld.prototype.setupWebSockets = function() {
    var connect,
        conn,
        self = this,
        retries = TJvsTheWorld.Config.SocketConnectRetries,
        shouldRetry = true

    if (!window["WebSocket"]) {
        this.prepend(self.currentDateTime(), "Your browser does not support websockets")
        return
    }

    connect = function() {
        conn = new WebSocket((window.location.protocol == "https:" ? "wss://" : "ws://") + window.location.host + "/tjvstheworld_ws")
        self.conn = conn

        conn.onopen = function(evt) {
            self.prepend(self.currentDateTime(), "Connection established.")
            retries = TJvsTheWorld.Config.SocketConnectRetries
        }
        conn.onerror = function(evt) {
            shouldRetry = false
        }
        conn.onclose = function(evt) {
            self.prepend(self.currentDateTime(), "Connection to the server has been lost.")

            if (shouldRetry && retries-- > 0) {
                self.prepend(self.currentDateTime(), "Attempting to reconnect to the server...")
                setTimeout(connect, TJvsTheWorld.Config.SocketConnectRetryDelayMS)
            }
        }
        conn.onmessage = function(evt) {
            var data

            try {
                data = JSON.parse(evt.data)
            }
            catch (e) {
                console.log("could not parse data " + e)
                return
            }

            if (data.type == "overview") {
                self.handleOverview(data)
            } else if (data.type == "update") {
                self.handleUpdate(data)
            } else if (data.type == "draw_user_details" || data.type == "spoil_user_details") {
                self.handleUserDetails(data)
            } else if (data.type == "user_entries") {
                self.handleUserEntries(data)
            }

            self.setupStickyDivs()
        }
    }

    connect()
}

TJvsTheWorld.prototype.setupOnEvents = function() {
    var close = function() {
        $("#modal").hide()
        $("#modal-screen").hide()
        return false;
    }

    $("#modal > p.close > a").add("#modal-screen").click(close)
    $("a[class^='username-']").on("click", { self: this }, TJvsTheWorld.requestUserDetails)
    $("#show-recent-activities-graph").on("click", { self: this }, TJvsTheWorld.requestUserEntriesGraph)
}

TJvsTheWorld.prototype.handleOverview =  function(data) {
    var i, n
    if (data.spoilers) {
        n = data.spoilers.length
        for (i = 0; i < n; i++) {
            this.spoilers[data.spoilers[i].username] = data.spoilers[i]
        }
    }

    this.updateStats(data)
    this.updateSpoilerLeaderboard()

    if (data.recentActivity) {
        n = data.recentActivity.length
        for (i=n-1; i>=0; i--) {
            this.insertUpdate(data.recentActivity[i], true)
        }
    }
}

TJvsTheWorld.prototype.handleUpdate = function(data) {
    if (data.action == "draw") {
        data.lastOdds = {
            "odds": data.odds,
            "datetime": data.datetime
        }
    }

    this.updateStats(data)

    if (this.spoilers.hasOwnProperty(data.username)) {
        this.spoilers[data.username].count++
        this.updateSpoilerLeaderboard()
    }

    this.insertUpdate(data)
}

TJvsTheWorld.Graph = function(config) {
    config = config || {}

    var now

    this._config = {
        shaded: config.shaded != undefined ? config.shaded : true,
        drawPoints: config.drawPoints != undefined ? config.drawPoints : true,
        legend: config.legend != undefined ? config.legend : false,
    }

    this.nTimelines = 0
    this.startDate = new Date(Date.UTC(2016, 11, 27, 0, 0, 0))
    this.endDate = new Date(Date.UTC(2017, 0, 14, 4, 0, 0))

    now = new Date()
    this.now = new Date(Date.UTC(now.getYear() + 1900, now.getMonth(), now.getDate()))

    this.dataSet = new vis.DataSet()
    this.groups = new vis.DataSet()
}

TJvsTheWorld.Graph.prototype.addGroup = function(id, label) {
    var opts = {
        id: id,
        content: label,
        options: {
            shaded: this._config.shaded ? "bottom" : false,
            drawPoints: this._config.drawPoints
        }
    }

    if (label) {
        opts.className = "vis-username-" + label.toLowerCase().replace(/\s+/, "-")
    }

    this.groups.add(opts)
}


TJvsTheWorld.Graph.prototype.addTimeline = function(timeline, timelineName) {
    var group = this.nTimelines,
        items = [],
        count

    this.nTimelines++

    for (s = new Date(this.startDate); s.getTime() <= this.now.getTime(); s = new Date(s.setDate(s.getDate() + 1))) {
        count = timeline[parseInt(s.getTime()/1000, 10)] || 0
        items.push({ x: s.toISOString().slice(0,10), y: count, group: group })
    }

    this.addGroup(group, timelineName)
    this.dataSet.add(items)
}

TJvsTheWorld.Graph.prototype.render = function($elem) {
    var graphHeight = "400px",
        opts

    $elem.html("")

    if (window.matchMedia('screen and (orientation:landscape) and (max-width: 799px)').matches) {
        graphHeight = "150px"
    } else if (window.matchMedia('screen and (max-width: 799px)').matches) {
        graphHeight = "300px"
    }

    opts = {
        dataAxis: {
            showMinorLabels: false,
            left: {
                format: function(v) {
                    return parseInt(v,10)
                },
                range: {
                    min: 0
                }
            }
        },
        end: this.endDate.toISOString().slice(0,10),
        height: graphHeight,
        interpolation: false,
        moment: function(date) {
            return vis.moment(date).utc();
        },
        moveable: false,
        showCurrentTime: false,
        start: this.startDate.toISOString().slice(0,10),
        timeAxis: {
            scale: "day",
            step: 1
        },
        width: "100%"
    }

    if (this._config.legend) {
        opts.legend = {
            enabled: true,
            left: {
                position: 'top-right'
            }
        }
    }

    new vis.Graph2d($elem.get(0), this.dataSet, this.groups, opts)
}

TJvsTheWorld.prototype.handleUserEntries = function(data) {
    var user, counts, items,
        $graph = $("#graph"),
        $modal = $("#modal"),
        g,
        users = [],
        nUsers = 0,
        i

    $modal.find("section").html("")
    $graph.html("")
    $modal.find("h2").html("All Activity")

    g = new TJvsTheWorld.Graph({shaded: false, drawPoints: false, legend: true})
    for (user in data.timelines) {
        if (!data.timelines.hasOwnProperty(user)) {
            continue
        }

        users.push(user)
    }

    users.sort()
    nUsers = users.length

    for (i = 0; i < nUsers; i++) {
        user = users[i]
        g.addTimeline(data.timelines[user], user)
    }

    g.render($("#graph"))

    $("#modal-screen").show()
    $modal.show()
}

TJvsTheWorld.prototype.handleUserDetails = function(data) {
    var $modal = $("#modal"),
        $graph = $("#graph"),
        rows = [],
        $table, $tr, i, n

    $modal.find("h2").html( this.usernameLink(data) )
    $graph.html("")

    if (data.type == "draw_user_details" ) {
        rows.push( [ "Draw Entries", data.drawCount ] )
        rows.push( [ "Last 24 Hours", data.drawCountInLast24 ] )
        rows.push( [ "Odds Improvers Hit",  data.oddsImproversCount ] )
    } else {
        rows.push( [ "Spoiler Entries", data.spoilersCount ] )
        rows.push( [ "Last 24 Hours", data.spoilersCountInLast24 ] )
        rows.push( [ "Multipliers Hit",  data.multipliersCount ] )
    }

    if (data.timeline && typeof(data.timeline) == "object") {
        var g = new TJvsTheWorld.Graph({ drawPoints: false })
        g.addTimeline(data.timeline, data.username)
        g.render($("#graph"))
    }

    n = rows.length

    $table = $("<table>")
    for (i = 0; i < n; i++) {
        $tr = $("<tr>")
        $tr.append("<td>" + rows[i][0] + "</td>")
        $tr.append("<td>" + rows[i][1] + "</td>")

        $table.append($tr)
    }

    $modal.find("section").html($table)

    $("#modal-screen").show()
    $modal.show()
}

TJvsTheWorld.prototype.setupStickyDivs = function() {
    if (this.stickyDivsWereSetup) {
        return
    }

    var $w = $(window),
        $odds = $("#current-odds"),
        elemTop = $odds.offset().top,
        $r = $("#replace-div"),
        toggle = ""

    $w.scroll(function() {
        if ($w.scrollTop() > elemTop) {
            if (toggle != "stick") {
                $r.height($odds.outerHeight())
                $odds.addClass("sticky")
                toggle = "stick"
            }
        } else {
            if (toggle != "unstick") {
                $odds.removeClass("sticky")
                $r.height(0)
                toggle = "unstick"
            }
        }
    })

    this.stickyDivsWereSetup = true
}

TJvsTheWorld.prototype.prepend = function(datetime, str, onInitialLoad) {
    var $elem = $("<p>"),
        $dtSpan, $msgSpan
    var n = $("#updates").find("p").length
    if (n % 2 == 0) {
        $elem.addClass("even")
    }

    if (datetime) {
        $dtSpan = $("<span>")
        $dtSpan.addClass("datetime")
        $dtSpan.html(datetime)
        $elem.append($dtSpan)
    }

    $msgSpan = $("<span>")
    $msgSpan.addClass("message")
    $msgSpan.html(str)
    $elem.append($msgSpan)

    $elem.find("a").on("click", { self: this }, TJvsTheWorld.requestUserDetails)

    $("#updates").prepend($elem)
    if (!onInitialLoad) {
        $elem.css("display", "none")
        $elem.show("slow")
    }

    return $elem
}

TJvsTheWorld.prototype.insertUpdate = function(data, onInitialLoad) {
    var $username = this.usernameLink(data),
        spoilMultiplierText = "",
        hash = this.actionHash(data),
        drawText, spoilText, $elem

    // only insert once
    //
    if ( this.seenActions[hash] != undefined ) {
        return
    }
    this.seenActions[hash] = true

    if (data.action == "draw") {
        drawText = $username + " attempted to draw a winner with " + fmtNumber(data.odds) + ":1 odds against"
        if (data.hasTjWon) {
            drawText += " and won!"
        } else {
            drawText += " and lost."
        }

        this.prepend(data.datetime, drawText, onInitialLoad)
        if (data.hasTjWon) {
            $elem = this.prepend(data.datetime, $username + " has won. This contest has now ended.", onInitialLoad)
            $elem.addClass("won")
        }
    } else if (data.action == "remove") {
        this.prepend(data.datetime, $username + " hit the " + parseInt(data.spoilersRemovedPerc*100,10) + "% odds improver and improved odds to " + fmtNumber(data.odds) + ":1 by removing " + fmtNumber(data.spoilersRemovedCount) + " spoiler entries", onInitialLoad)
    } else {
        if (data.spoilMultiplier > 1) {
            spoilMultiplierText = " hit a spoil multiplier of " + data.spoilMultiplier + " and"
        }

        spoilText = $username + spoilMultiplierText + " decreased the odds of TJ winning to " + fmtNumber(data.odds) + ":1 against."
        this.prepend(data.datetime, spoilText, onInitialLoad)
    }
}

TJvsTheWorld.prototype.updateSpoilerLeaderboard = function() {
    var self = this,
        sarr = [],
        $table = $("<table>"),
        totalEntries = 0,
        $tr,
        perc, i, s

    for (s in this.spoilers) {
        if (this.spoilers.hasOwnProperty(s)) {
            sarr.push(this.spoilers[s])
        }

        totalEntries += this.spoilers[s].count
    }

    sarr.sort(function(a,b) {
        return b.count - a.count
    })

    n = sarr.length

    for (i = 0; i < n; i++) {
        $tr = $("<tr>")
        $tr.append("<td class=\"rank\">" + (i+1) + "</td>")
        $tr.append("<td class=\"username\">" + this.usernameLink(sarr[i]) + "</td>")
        $tr.append("<td class=\"count\">" + fmtNumber(sarr[i].count) + "</td>")
        $tr.append("<td class=\"perc\">" + (100*sarr[i].count/totalEntries).toFixed(3) + "%</td>")

        $table.append($tr)
    }

    $table.find("a").on("click", { self: this }, TJvsTheWorld.requestUserDetails)

    $("#spoiler-leaderboard").html($table)
}

TJvsTheWorld.requestUserDetails = function(event) {
    event.data.self.conn.send(JSON.stringify({
        action: "user_details",
        id: parseInt(this.getAttribute("data-id"), 10),
    }))
    return false;
}

TJvsTheWorld.requestUserEntriesGraph = function(event) {
    event.data.self.conn.send(JSON.stringify({
        action: "user_entries",
    }))
    return false;
}

TJvsTheWorld.prototype.updateStats = function(data) {
    $("#spoiler-count").html(fmtNumber(data.uniqueSpoilerCount))
    $("#spoiler-and-multipliers").html(fmtNumber(data.spoilerCount))
    $("#draw-count").html(fmtNumber(data.drawCount))
    $("#total-entries").html(fmtNumber(data.drawCount + data.uniqueSpoilerCount))
    $("#current-odds").html(fmtNumber(data.odds) + ":1")

    if (data.spoilersRemovedCount != undefined) {
        if (typeof(this.spoilersRemovedCount) === "undefined" || data.type === "overview") {
            this.spoilersRemovedCount = data.spoilersRemovedCount
        } else {
            this.spoilersRemovedCount += data.spoilersRemovedCount
        }
        $("#spoilers-removed").html(fmtNumber(this.spoilersRemovedCount))
    }

    if (data.hasTjWon) {
        $("#has-tj-won").html("Yes")
    } else {
        $("#has-tj-won").html("No")
    }

    if (data.hasOwnProperty("lastOdds")) {
        $("#last-odds").html(fmtNumber(data.lastOdds.odds) + ":1 (" + data.lastOdds.datetime + ")")
    }
}

TJvsTheWorld.prototype.usernameLink = function(data) {
    var usernameClass = "username-" + data.username.toLowerCase().replace(/\s+/, "-")
    return "<a href=\"#\" data-id=\"" + data.userID + "\" class=\"username " + usernameClass + "\">" + data.username + "</a>"
}

TJvsTheWorld.prototype.actionHash = function(data) {
    return data.action + ":" + data.userID + ":" + data.datetime
}

TJvsTheWorld.prototype.currentDateTime = function() {
    var pad = function(d) {
        if (d < 10) {
            return "0" + d
        }

        return d
    }
    var n = new Date()
    var h = n.getHours()
    var ampm = "am"
    if (h > 12) {
        ampm = "pm"
        h -= 12
    } else if (h == 0) {
        h = 12
    }

    return (n.getMonth() + 1) + "/" + n.getDate() + " " + h + ":" + pad(n.getMinutes()) + ":" + pad(n.getSeconds()) + " " + ampm
}

window.onload = function() {
    new TJvsTheWorld()
}
