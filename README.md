# tjvstheworld

A Telegram bot for managing the TJ vs. The World Sweepstakes, an online sweepstakes resricted to a group of friends.

## Requirements

* Database - Any database which supports windowing and common table expressions (CTE) should be acceptable. Code was tested with PostgreSQL 9.5.x.
* Go 1.7.x+
* You'll also need to write a main package which will setup the handlers used by `tjvstheworldbot`. See [github.com/weters/telegram](https://github.com/weters/telegram) for more information.

## Code Layout

* `tjvstheworld.go` - Entry point into the Telegram bot. Initial setup and configuration.
* `controller.go` - Controller logic.
* `model/model.go` - Model logic.
* `tjlog/tjlog.go` - Package for a second "default" logger.
* `ws.go` - Logic for handling the WebSockets, which sends data to the statistics page on the web.
* `static/` - Contains static files used for the statistics page (HTML, JS, CSS).
* `welcome/welcome.go` - Command which sends the initial welcome message when the contest starts.
* `migrations.sql` - [Liquibase](http://www.liquibase.org) migrations file.

# Miscellaneous

There are three secret phrases. This first secret phrase is `tracy klima`. The next two won't be as easy.
