--liquibase formatted sql

--changeset tpeters:1 splitStatements:false

CREATE TABLE draws (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    username TEXT,
    winner BOOLEAN NOT NULL DEFAULT 'f',
    current_odds INTEGER NOT NULL,
    created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'utc')
);

CREATE INDEX draws_user_id_username_created_idx ON draws (user_id, username, created);
CREATE INDEX draws_winner_idx ON draws (winner);

CREATE FUNCTION draw(_user_id integer, _username text, _winner boolean, _current_odds integer, _wait integer, OUT _ok boolean, OUT _duration float) 
    LANGUAGE plpgsql
    AS $$
DECLARE
    _diff float;
    _created timestamp without time zone;
BEGIN
    LOCK TABLE draws IN ACCESS EXCLUSIVE MODE;

    SELECT INTO _created created
    FROM draws
    WHERE user_id = _user_id
    ORDER BY created DESC
    LIMIT 1;

    IF FOUND THEN
        _diff = EXTRACT(EPOCH FROM (NOW() AT TIME ZONE 'UTC' - _created));
        IF _diff < _wait THEN
            _ok = FALSE;
            _duration = _wait - _diff;
            RETURN;
        END IF;
    END IF;

    INSERT INTO draws (user_id, username, winner, current_odds) VALUES (_user_id, _username, _winner, _current_odds);
    _ok = TRUE;
    _duration = 0;
END;
$$;

CREATE TABLE spoilers (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    username TEXT,
    created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'utc')
);

CREATE INDEX spoilers_user_id_created_idx ON spoilers (user_id, created);
CREATE INDEX spoilers_username_idx ON spoilers (username);

CREATE FUNCTION spoil(_user_id INTEGER, _username TEXT, _wait INTEGER, OUT _ok boolean, OUT _duration float)
    LANGUAGE plpgsql
    AS $$
DECLARE
    _diff float;
    _created timestamp without time zone;
BEGIN
    PERFORM 1
    FROM spoilers
    WHERE user_id = _user_id
    LIMIT 1
    FOR UPDATE;

    SELECT INTO _created created
    FROM spoilers
    WHERE user_id = _user_id
    ORDER BY created DESC
    LIMIT 1;

    IF FOUND THEN
        _diff = EXTRACT(EPOCH FROM (NOW() AT TIME ZONE 'UTC' - _created));
        IF _diff < _wait THEN
            _ok = FALSE;
            _duration = _wait - _diff;
            RETURN;
        END IF;
    END IF;

    INSERT INTO spoilers (user_id, username) VALUES (_user_id, _username);
    _ok = TRUE;
    _duration = 0;
END;
$$;

--rollback DROP TABLE spoilers;
--rollback DROP TABLE draws;
--rollback DROP FUNCTION spoil(int, text, int);
--rollback DROP FUNCTION draw(int, text, boolean, int, int);

--changeset tpeters:2 splitStatements:false

ALTER TABLE spoilers ADD COLUMN current_odds INTEGER;
UPDATE spoilers SET current_odds = (id * 50 + 100);

CREATE FUNCTION spoil_v2(_user_id INTEGER, _username TEXT, _current_odds INTEGER, _wait INTEGER, OUT _ok boolean, OUT _duration float)
    LANGUAGE plpgsql
    AS $$
DECLARE
    _diff float;
    _created timestamp without time zone;
BEGIN
    PERFORM 1
    FROM spoilers
    WHERE user_id = _user_id
    LIMIT 1
    FOR UPDATE;

    SELECT INTO _created created
    FROM spoilers
    WHERE user_id = _user_id
    ORDER BY created DESC
    LIMIT 1;

    IF FOUND THEN
        _diff = EXTRACT(EPOCH FROM (NOW() AT TIME ZONE 'UTC' - _created));
        IF _diff < _wait THEN
            _ok = FALSE;
            _duration = _wait - _diff;
            RETURN;
        END IF;
    END IF;

    INSERT INTO spoilers (user_id, username, current_odds) VALUES (_user_id, _username, _current_odds);
    _ok = TRUE;
    _duration = 0;
END;
$$;

--rollback ALTER TABLE spoilers DROP COLUMN current_odds;
--rollback DROP FUNCTION spoil_v2(INTEGER, TEXT, INTEGER, INTEGER);

--changeset tpeters:3 splitStatements:false

CREATE TABLE spoiler_removers (
    id SERIAL PRIMARY KEY,
    user_id INTEGER NOT NULL,
    username TEXT NOT NULL,
    percentage REAL NOT NULL,
    removed INTEGER NOT NULL,
    current_odds INTEGER NOT NULL,
    created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC')
);

--rollback DROP TABLE spoiler_removers;

--changeset tpeters:4 splitStatements:false

ALTER TABLE spoilers ADD COLUMN multiplier INTEGER NOT NULL DEFAULT 1;

CREATE FUNCTION spoil_v3(_user_id INTEGER, _username TEXT, _current_odds INTEGER, _multiplier INTEGER, _wait INTEGER, OUT _ok boolean, OUT _duration float)
    LANGUAGE plpgsql
    AS $$
DECLARE
    _diff float;
    _created timestamp without time zone;
BEGIN
    PERFORM 1
    FROM spoilers
    WHERE user_id = _user_id
    LIMIT 1
    FOR UPDATE;

    SELECT INTO _created created
    FROM spoilers
    WHERE user_id = _user_id
    ORDER BY created DESC
    LIMIT 1;

    IF FOUND THEN
        _diff = EXTRACT(EPOCH FROM (NOW() AT TIME ZONE 'UTC' - _created));
        IF _diff < _wait THEN
            _ok = FALSE;
            _duration = _wait - _diff;
            RETURN;
        END IF;
    END IF;

    INSERT INTO spoilers (user_id, username, current_odds, multiplier) VALUES (_user_id, _username, _current_odds, _multiplier);
    _ok = TRUE;
    _duration = 0;
END;
$$;

--rollback ALTER TABLE spoilers DROP COLUMN multiplier;
--rollback DROP FUNCTION spoil_v3(INTEGER, TEXT, INTEGER, INTEGER, INTEGER);

--changeset tpeters:5

ALTER TABLE draws ALTER COLUMN user_id TYPE bigint;
ALTER TABLE spoilers ALTER COLUMN user_id TYPE bigint;
ALTER TABLE spoiler_removers ALTER COLUMN user_id TYPE bigint;

--changeset tpeters:6 splitStatements:false

DROP FUNCTION IF EXISTS draw(INTEGER, TEXT, BOOLEAN, INTEGER, INTEGER);
CREATE OR REPLACE FUNCTION draw(_user_id integer, _username text, _winner boolean, _current_odds integer, _wait integer, OUT _ok boolean, OUT _duration float) 
    LANGUAGE plpgsql
    AS $$
DECLARE
    _diff float;
    _created timestamp without time zone;
BEGIN
    LOCK TABLE draws IN ACCESS EXCLUSIVE MODE;

    SELECT INTO _created created
    FROM draws
    WHERE user_id = _user_id
    ORDER BY created DESC
    LIMIT 1;

    IF FOUND THEN
        _diff = EXTRACT(EPOCH FROM (NOW() AT TIME ZONE 'UTC' - _created));
        IF _diff < _wait THEN
            _ok = FALSE;
            _duration = _wait - _diff;
            RETURN;
        END IF;
    END IF;

    INSERT INTO draws (user_id, username, winner, current_odds) VALUES (_user_id, _username, _winner, _current_odds);
    _ok = TRUE;
    _duration = 0;
END;
$$;

DROP FUNCTION IF EXISTS spoil(INTEGER, TEXT, INTEGER);
CREATE OR REPLACE FUNCTION spoil(_user_id BIGINT, _username TEXT, _wait INTEGER, OUT _ok boolean, OUT _duration float)
    LANGUAGE plpgsql
    AS $$
DECLARE
    _diff float;
    _created timestamp without time zone;
BEGIN
    PERFORM 1
    FROM spoilers
    WHERE user_id = _user_id
    LIMIT 1
    FOR UPDATE;

    SELECT INTO _created created
    FROM spoilers
    WHERE user_id = _user_id
    ORDER BY created DESC
    LIMIT 1;

    IF FOUND THEN
        _diff = EXTRACT(EPOCH FROM (NOW() AT TIME ZONE 'UTC' - _created));
        IF _diff < _wait THEN
            _ok = FALSE;
            _duration = _wait - _diff;
            RETURN;
        END IF;
    END IF;

    INSERT INTO spoilers (user_id, username) VALUES (_user_id, _username);
    _ok = TRUE;
    _duration = 0;
END;
$$;

DROP FUNCTION IF EXISTS spoil_v2(INTEGER, TEXT, INTEGER, INTEGER);
CREATE OR REPLACE FUNCTION spoil_v2(_user_id BIGINT, _username TEXT, _current_odds INTEGER, _wait INTEGER, OUT _ok boolean, OUT _duration float)
    LANGUAGE plpgsql
    AS $$
DECLARE
    _diff float;
    _created timestamp without time zone;
BEGIN
    PERFORM 1
    FROM spoilers
    WHERE user_id = _user_id
    LIMIT 1
    FOR UPDATE;

    SELECT INTO _created created
    FROM spoilers
    WHERE user_id = _user_id
    ORDER BY created DESC
    LIMIT 1;

    IF FOUND THEN
        _diff = EXTRACT(EPOCH FROM (NOW() AT TIME ZONE 'UTC' - _created));
        IF _diff < _wait THEN
            _ok = FALSE;
            _duration = _wait - _diff;
            RETURN;
        END IF;
    END IF;

    INSERT INTO spoilers (user_id, username, current_odds) VALUES (_user_id, _username, _current_odds);
    _ok = TRUE;
    _duration = 0;
END;
$$;

DROP FUNCTION IF EXISTS spoil_v3(INTEGER, TEXT, INTEGER, INTEGER, INTEGER);
CREATE OR REPLACE FUNCTION spoil_v3(_user_id BIGINT, _username TEXT, _current_odds INTEGER, _multiplier INTEGER, _wait INTEGER, OUT _ok boolean, OUT _duration float)
    LANGUAGE plpgsql
    AS $$
DECLARE
    _diff float;
    _created timestamp without time zone;
BEGIN
    PERFORM 1
    FROM spoilers
    WHERE user_id = _user_id
    LIMIT 1
    FOR UPDATE;

    SELECT INTO _created created
    FROM spoilers
    WHERE user_id = _user_id
    ORDER BY created DESC
    LIMIT 1;

    IF FOUND THEN
        _diff = EXTRACT(EPOCH FROM (NOW() AT TIME ZONE 'UTC' - _created));
        IF _diff < _wait THEN
            _ok = FALSE;
            _duration = _wait - _diff;
            RETURN;
        END IF;
    END IF;

    INSERT INTO spoilers (user_id, username, current_odds, multiplier) VALUES (_user_id, _username, _current_odds, _multiplier);
    _ok = TRUE;
    _duration = 0;
END;
$$;

--changeset tpeters:7 splitStatements:false

CREATE TABLE secret_phrases (
    id SERIAL PRIMARY KEY,
    phrase TEXT UNIQUE,
    user_id BIGINT NOT NULL,
    username TEXT,
    created TIMESTAMP WITHOUT TIME ZONE NOT NULL DEFAULT (NOW() AT TIME ZONE 'UTC')
);

--rollback DROP TABLE secret_phrases;
