// Package tjlog handles logging of actions to a separate log file.
package tjlog

import (
	"fmt"
	"log"
	"os"
)

var logger *log.Logger

// Fatal is equivalent to Print() followed by a call to os.Exit(1).
func Fatal(v ...interface{}) {
	logger.Fatal(v...)
}

// Fatalf is equivalent to Printf() followed by a call to os.Exit(1).
func Fatalf(fmt string, v ...interface{}) {
	logger.Fatalf(fmt, v...)
}

// Fatalln is equivalent to Println() followed by a call to os.Exit(1).
func Fatalln(v ...interface{}) {
	logger.Fatalln(v...)
}

// Panic is equivalent to Print() followed by a call to panic().
func Panic(v ...interface{}) {
	logger.Panic(v...)
}

// Panicf is equivalent to Printf() followed by a call to panic().
func Panicf(fmt string, v ...interface{}) {
	logger.Panicf(fmt, v...)
}

// Panicln is equivalent to Println() followed by a call to panic().
func Panicln(v ...interface{}) {
	logger.Panicln(v...)
}

// Print calls Output to print to the standard logger. Arguments are
// handled in the manner of fmt.Print.
func Print(v ...interface{}) {
	logger.Print(v...)
}

// Printf calls Output to print to the standard logger. Arguments are
// handled in the manner of fmt.Printf.
func Printf(fmt string, v ...interface{}) {
	logger.Printf(fmt, v...)
}

// Println calls Output to print to the standard logger. Arguments are
// handled in the manner of fmt.Println.
func Println(v ...interface{}) {
	logger.Println(v...)
}

// Setup will setup a log that outputs to the specified filename.
func Setup(filename string) error {
	file, err := os.OpenFile(filename, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0666)
	if err != nil {
		return err
	}

	logger = log.New(file, fmt.Sprintf("[%d] ", os.Getpid()), log.Ldate|log.Ltime|log.Llongfile)
	return nil
}
