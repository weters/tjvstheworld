package tjvstheworld

import (
	"fmt"
	"log"
	"strings"
	"time"

	"github.com/weters/telegram/bot"
	"gitlab.com/weters/tjvstheworld/model"
)

const rulesURL = "https://tompeters.me/tvtws-1.1.pdf"
const liveStatsURL = "https://tompeters.me:8443/tjvstheworld/"

// Controller handles all of the various messages sent via Telegram
type Controller struct {
	Model *model.Model
}

// NewController instantiates a new controller object
func NewController(m *model.Model) *Controller {
	return &Controller{
		Model: m,
	}
}

// DefaultHandler handles messages from Telegram to this bot that do not have a command
// associated with it.
func (c *Controller) DefaultHandler(b *bot.Bot, u *bot.UpdateResponse, args string) {
	// this _shouldn't_ happen, but check just to make sure
	if u.IsGroup() {
		return
	}

	now := time.Now()
	if now.Before(PeriodStart) {
		c.sendResponse(b, u, fmt.Sprintf("The Sweeptakes do not start until %s", PeriodStart.Format("3:04 PM MST on January _2, 2006")))
		return
	} else if now.After(PeriodEnd) {
		c.sendSweepstakesEndedMessage(b, u)
		return
	} else if c.Model.Sweepstakes.HasWinner() {
		c.sendSweepstakesEndedMessage(b, u)
		return
	}

	_, ok := conf.UserIDs[u.FromID()]
	if !ok {
		c.sendResponse(b, u, "You are not eligible to participate in this Sweepstakes.")
		return
	}

	if u.FromID() == TargetID {
		c.handleTJMessage(b, u)
		return
	}

	c.handleWorldMessage(b, u)
}

// RulesHandler handles /rules command.
func (c *Controller) RulesHandler(b *bot.Bot, u *bot.UpdateResponse, args string) {
	text := fmt.Sprintf(`*Abbreviated Rules*

1. Sweepstakes run 12/27 - 1/28.

2. TJ is allowed to message once per minute. Every valid message will randomly determine if he instant wins or not. Odds on any given message start at 100:1 against.

3. Everyone else ("The World") is allowed to message once per every five seconds. Each valid message will decrease odds of TJ winning by 100.

4. If TJ doesn't win within the Sweepstakes Period, someone from The World will randomly win. The more entries a participant from The World entered, the greater chance they have of winning.

For the Official Rules, please visit %s`, rulesURL)

	if _, err := b.PostSendMessage(&bot.SendMessage{
		ChatID: u.ChatID(),
		Text:   text,
		DisableWebPagePreview: true,
		DisableNotification:   true,
		ParseMode:             "markdown",
	}); err != nil {
		log.Printf("error: could not post send message: %s\n", err)
		return
	}
}

// HelpHandler handles the /help command.
func (c *Controller) HelpHandler(b *bot.Bot, u *bot.UpdateResponse, args string) {
	text := fmt.Sprintf(`This bot is used to conduct the offical TJ vs. The World Sweepstakes. For offical rules, please see: %s

Commands:

/rules - Show Sweepstakes rules
/statistics - Show current statistics
/practicedraw - Draw a random winner (practice)`, rulesURL)

	if _, err := b.PostSendMessage(&bot.SendMessage{
		ChatID: u.ChatID(),
		Text:   text,
		DisableWebPagePreview: true,
		DisableNotification:   true,
	}); err != nil {
		log.Printf("error: could not post send message: %s\n", err)
		return
	}
}

// StatisticsHandler handles the /statistics command.
func (c *Controller) StatisticsHandler(b *bot.Bot, u *bot.UpdateResponse, args string) {
	_, ok := conf.UserIDs[u.ChatID()]
	if u.ChatID() != ChatID && !ok {
		return
	}

	msg := fmt.Sprintf("Real-time statistics: %s", liveStatsURL)
	_, err := b.PostSendMessage(&bot.SendMessage{
		ChatID:                u.ChatID(),
		Text:                  msg,
		ParseMode:             "markdown",
		DisableWebPagePreview: true,
		DisableNotification:   true,
	})

	if err != nil {
		log.Printf("error: could not post send message: %s\n", err)
		return
	}
}

// PracticeDrawHandler will pretend to draw a winner in the event TJ loses.
func (c *Controller) PracticeDrawHandler(b *bot.Bot, u *bot.UpdateResponse, args string) {
	w, err := c.Model.SelectRandomDrawWinner()
	if err != nil {
		log.Printf("error: could not practice select random draw winner: %s\n", err)
		return
	}

	msg := fmt.Sprintf("In a practice random drawing, %s won! They had a %.3f%% chance of winning.", w.Username, w.Perc)
	if _, err := b.PostSendMessage(&bot.SendMessage{
		ChatID:              u.ChatID(),
		Text:                msg,
		DisableNotification: true,
	}); err != nil {
		log.Printf("error: could not post send message: %s\n", err)
		return
	}
}

// handleTJMessage will take care of the message sent by TJ.
func (c *Controller) handleTJMessage(b *bot.Bot, u *bot.UpdateResponse) {
	t := strings.ToLower(u.Message.Text)
	if t == "" {
		return
	} else if t == "spoil" {
		c.handleWorldMessage(b, u)
		return
	} else if t != "draw" {
		c.trySecretPhrase(b, u)
		return
	}

	if time.Now().Before(DrawPeriodStart) {
		c.sendResponse(b, u, fmt.Sprintf("The Instant Win portion of the Sweeptakes do not start until %s", DrawPeriodStart.Format("3:04 PM MST on January _2, 2006")))
		return
	}

	ok, winner, waitDuration, err := c.Model.Sweepstakes.Draw(u.FromID(), u.Message.From.DisplayName())
	odds := c.Model.Sweepstakes.GetOdds()
	if err != nil {
		if err == model.ErrWinnerExists {
			c.sendSweepstakesEndedMessage(b, u)
			return
		}

		log.Printf("error: could not increment spoiler counter: %s\n", err)
		c.sendResponse(b, u, "error: could not execute command. please contact Tom Peters")
		return
	}

	if !ok {
		c.userMustWait(b, u, waitDuration)
		return
	}

	var impOdds *model.ImprovedOdds
	if winner {
		if _, err := b.PostSendMessage(&bot.SendMessage{
			ChatID: ChatID,
			Text:   "TJ has won the grand prize. This contest has now ended.",
		}); err != nil {
			log.Printf("error: could not post send message: %s\n", err)
		}

		c.sendResponse(b, u, fmt.Sprintf("Congratulations! You have won the grand prize. Your odds were %d:1 against. Please contact Tom Peters to claim your prize.", odds))
	} else {
		var err error
		impOdds, err = c.Model.Sweepstakes.AttemptToImproveOdds(u.FromID(), u.Message.From.DisplayName())
		if err != nil {
			log.Printf("error: could not attempt to improve odds: %s\n", err)
		}

		if impOdds.DidImprove {
			c.sendResponse(b, u, fmt.Sprintf("Sorry! You did not win. On the bright side, you were able to improve your odds by %d%% and you effectively removed %d spoiler entries!", int(100*impOdds.RemovedPerc), impOdds.RemovedCount))
		} else {
			c.sendResponse(b, u, "Sorry! You did not win. Please try again.")
		}
	}

	c.updateStatistics(b, u, model.ActionTypeDraw, odds, impOdds, nil)
}

// handleWorldMessage will take care of the messages sent by The World.
func (c *Controller) handleWorldMessage(b *bot.Bot, u *bot.UpdateResponse) {
	t := strings.ToLower(u.Message.Text)
	if t == "" {
		return
	} else if t != "spoil" {
		c.trySecretPhrase(b, u)
		return
	}

	ok, multiplier, waitDuration, err := c.Model.Sweepstakes.Spoil(u.FromID(), u.Message.From.DisplayName())
	if err != nil {
		log.Printf("error: could not increment spoiler counter: %s\n", err)
		c.sendResponse(b, u, "error: could not execute command. please contact Tom Peters")
		return
	}

	if !ok {
		c.userMustWait(b, u, waitDuration)
		return
	}

	odds := c.Model.Sweepstakes.GetOdds()
	response := fmt.Sprintf("TJ's odds have decreased to %d:1 against", odds)
	if multiplier > 1 {
		response = fmt.Sprintf("Congrats, you hit a multiplier worth %d spoils! %s", multiplier, response)
	}

	c.sendResponse(b, u, response)
	c.updateStatistics(b, u, model.ActionTypeSpoil, odds, nil, func(u *model.WsUpdate) {
		u.SpoilMultiplier = multiplier
	})
}

// sendResponse is a generic method for sending a response to the Discussion chat.
// Web page previews will be disabled, as well as notifications.
func (c *Controller) sendResponse(b *bot.Bot, u *bot.UpdateResponse, s string) {
	if _, err := b.PostSendMessage(&bot.SendMessage{
		ChatID: u.ChatID(),
		Text:   s,
		DisableWebPagePreview: true,
		DisableNotification:   true,
	}); err != nil {
		log.Printf("error: could not post send message: %s\n", err)
		return
	}
}

// sendSweepstakesEndedMessage sends a content has ended message.
func (c *Controller) sendSweepstakesEndedMessage(b *bot.Bot, u *bot.UpdateResponse) {
	c.sendResponse(b, u, "This contest has ended.")
	return
}

// updateStatistics will try to update an existing statistics message. If an existing one isn't found,
// it will send a new one.
func (c *Controller) updateStatistics(b *bot.Bot, u *bot.UpdateResponse, at model.ActionType, odds int, impOdds *model.ImprovedOdds, cb func(*model.WsUpdate)) {
	s := c.Model.Sweepstakes
	sc := s.SpoilerCount()
	usc := s.UniqueSpoilerCount()
	dc := s.DrawCount()
	hw := s.HasWinner()

	wsData := model.NewWsUpdate(at, odds, u.FromID(), u.Message.From.DisplayName(), sc, usc, dc, hw)
	if cb != nil {
		cb(wsData)
	}
	SendData(wsData)

	if impOdds != nil && impOdds.DidImprove {
		wsDataCopy := *wsData
		wsDataCopy.Action = model.ActionTypeRemove
		wsDataCopy.SpoilersRemovedPerc = impOdds.RemovedPerc
		wsDataCopy.SpoilersRemovedCount = impOdds.RemovedCount
		wsDataCopy.Odds = impOdds.NewOdds
		SendData(wsDataCopy)
	}
}

// userMustWait is a method that will output a message notifying the user they need to wait waitDuration more seconds
// until their entry will be accepted.
func (c *Controller) userMustWait(b *bot.Bot, u *bot.UpdateResponse, waitDuration float32) {
	log.Printf("%s needs to wait %0.1f more seconds (%f)", u.Message.From.DisplayName(), waitDuration, waitDuration)
	waitStr := fmt.Sprintf("%0.1f", waitDuration)
	if waitStr == "0.0" {
		waitStr = fmt.Sprintf("%0.3f", waitDuration)
	}
	c.sendResponse(b, u, fmt.Sprintf("Please wait %s more seconds", waitStr))

	return
}

func (c *Controller) trySecretPhrase(b *bot.Bot, u *bot.UpdateResponse) {
	sp := strings.ToLower(u.Message.Text)
	if !c.Model.SecretPhrases.IsSecretPhrase(sp) {
		c.sendResponse(b, u, "That is not a valid secret phrase")
		return
	}

	hit, err := c.Model.SecretPhrases.SubmitSecretPhrase(u.FromID(), u.Message.From.DisplayName(), sp)
	if err != nil {
		log.Printf("error: could not submit secret phrase %s: %s\n", sp, err)
		return
	}

	if !hit {
		c.sendResponse(b, u, "That secret phrase has already been found")
		return
	}

	c.sendResponse(b, u, "Congratulations! You found a secret phrase. Contact Tom Peters for your prize.")

	if _, err := b.PostSendMessage(&bot.SendMessage{
		ChatID: ChatID,
		Text:   fmt.Sprintf("%s has found the secret phrase: %s", u.Message.From.DisplayName(), sp),
	}); err != nil {
		log.Printf("error: could not post send message: %s\n", err)
	}

	return
}

func (c *Controller) sendUserDetailsToClient(uID int64, client *Client) {
	var d interface{}
	var err error

	if uID == TargetID {
		d, err = model.NewWsDrawUserDetailsForUserID(uID)
	} else {
		d, err = model.NewWsSpoilUserDetailsForUserID(uID)
	}

	if err != nil {
		log.Printf("error: could not retrieve user details for %d: %s\n", uID, err)
		return
	}

	client.send <- d
}

func (c *Controller) sendUserEntriesToClient(client *Client) {
	e, err := model.NewWsUserEntries()
	if err != nil {
		log.Printf("error: could not retrieve user entries: %s\n", err)
		return
	}

	client.send <- e
}
