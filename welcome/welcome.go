package main

import (
	"log"

	"github.com/weters/telegram/bot"
	"gitlab.com/weters/tele/teleconfig"
)

// ChatID is the ID of the Telegram discussion thread
const ChatID = -1001065406113

func main() {
	bConfig := tjVsTheWorldBot()
	tjBot := bot.New(bConfig.Name, bConfig.Token)

	res, err := tjBot.PostSendMessage(&bot.SendMessage{
		ChatID:    ChatID,
		Text:      "TJ vs The World Sweepstakes is now *LIVE*",
		ParseMode: "markdown",
	})

	if err != nil {
		log.Fatal(err)
	}

	if !res.OK {
		log.Fatalf("message could not be sent: %s", res.String())
	}

	log.Printf("message %d sent successfully.", res.Result.ID)
}

func tjVsTheWorldBot() *teleconfig.Bot {
	for _, b := range teleconfig.BotsConfig {
		if b.Name == "TJvsWORLDbot" {
			return b
		}
	}

	panic("could not find TJvsWORLDbot")
}
