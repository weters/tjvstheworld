package main

import (
	"crypto/rand"
	"flag"
	"fmt"
	"math/big"
	"os"
	"regexp"
	"strconv"
	"strings"
)

var ratioRx = regexp.MustCompile(`^[0-9]+:[0-9]+\z`)

func main() {
	ratioString := flag.String("ratio", "1:1", "World:TJ rate.")
	baseEntries := flag.Int("base", 50000, "the number of entries for the lowest team")
	iterations := flag.Int("iterations", 100, "how many times to run the simulation")
	removerBigOdds := flag.Int("remover-big-odds", 1000, "odds of hitting big remover")
	removerBig := flag.Float64("remover-big", 0.1, "percentage to remove on big hit")
	removerSmallOdds := flag.Int("remover-small-odds", 100, "odds of hitting small remover")
	removerSmall := flag.Float64("remover-small", 0.01, "percentage to remove on small hit")
	multiplierBigOdds := flag.Int("multiplier-big-odds", 1000, "odds of hitting big multiplier")
	multiplierBig := flag.Int("multiplier-big", 400, "big multiplier value")
	multiplierSmallOdds := flag.Int("multiplier-small-odds", 100, "odds of hitting small multiplier")
	multiplierSmall := flag.Int("multiplier-small", 25, "small multiplier value")
	flag.Parse()

	if !ratioRx.MatchString(*ratioString) {
		fmt.Fprintf(os.Stderr, "error: ratio must look like 1:1 or 3:2")
		os.Exit(1)
	}

	ratio := strings.Split(*ratioString, ":")
	worldRate, _ := strconv.Atoi(ratio[0])
	tjRate, _ := strconv.Atoi(ratio[1])

	loops := *baseEntries / worldRate
	if tjRate < worldRate {
		loops = *baseEntries / tjRate
	}

	totalOdds := 0
	for i := 0; i < *iterations; i++ {
		spoils := 0
		removedSpoils := 0

		for j := 0; j < loops; j++ {
			for k := 0; k < worldRate; k++ {
				if try(*multiplierBigOdds) {
					spoils += *multiplierBig
				} else if try(*multiplierSmallOdds) {
					spoils += *multiplierSmall
				} else {
					spoils += 1
				}
			}

			for k := 0; k < tjRate; k++ {
				actualSpoils := spoils - removedSpoils

				if try(*removerBigOdds) {
					removedSpoils += int(float64(actualSpoils) * *removerBig)
				} else if try(*removerSmallOdds) {
					removedSpoils += int(float64(actualSpoils) * *removerSmall)
				}
			}
		}

		totalOdds += (spoils-removedSpoils)*5 + 100
	}

	fmt.Println(totalOdds / *iterations)
}

func try(v int) bool {
	r, _ := rand.Int(rand.Reader, big.NewInt(int64(v)))
	return int(r.Int64()) == 0
}
