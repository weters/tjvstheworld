package model

import (
	"crypto/rand"
	"database/sql"
	"encoding/base64"
	"errors"
	"fmt"
	"log"
	"math/big"
	"sync"
	"time"

	"gitlab.com/weters/tjvstheworld/tjlog"
)

type oddsInt int

const baseOdds = 100
const spoilerMultiplier = 5
const spoilWaitDuration = 5
const drawWaitDuration = 5
const spoilerRemoveBigPercentage = 0.1
const spoilerRemoveSmallPercentage = 0.01
const spoilMultiplierSmall int = 25
const spoilMultiplierBig int = 400
const dateFormat = "1/2 3:04:05 pm"

const (
	oddsSpoilerRemoveBig     oddsInt = 1000
	oddsSpoilerRemoveSmall           = 100
	oddsSpoilMultiplierSmall         = 100
	oddsSpoilMultiplierBig           = 1000
)

// WsType represents the type of an object to be passed via websockets
type WsType string

// WsType constants
const (
	WsTypeUpdate           WsType = "update"
	WsTypeOverview                = "overview"
	WsTypeDrawUserDetails         = "draw_user_details"
	WsTypeSpoilUserDetails        = "spoil_user_details"
	WsTypeUserEntries             = "user_entries"
)

// ActionType represents the type of action that happened
type ActionType string

// ActionType constants
const (
	ActionTypeDraw   ActionType = "draw"
	ActionTypeSpoil             = "spoil"
	ActionTypeRemove            = "remove"
)

// Team is one of two teams
type Team string

// Team constants
const (
	TeamTJ       Team = "tj"
	TeamTheWorld      = "world"
)

// ErrWinnerExists is an error when a winner already exists.
var ErrWinnerExists = errors.New("model: a winner already exists")
var stmtRandomDrawEntries,
	stmtDraw,
	stmtDrawsCount,
	stmtDrawUserNameFromUserID,
	stmtOddsLastDraw,
	stmtRecentActivity,
	stmtSpoil,
	stmtSpoilUserNameFromUserID,
	stmtSpoilerRemover,
	stmtSpoilerRemoversSum,
	stmtSpoilersCount,
	stmtTopSpoilers,
	stmtUniqueSpoilersCount,
	stmtUserEntriesCounts,
	stmtUserDetailsDrawCountLast24,
	stmtUserDetailsDrawsByHour,
	stmtUserDetailsMultipliersCount,
	stmtUserDetailsOddsImproversCount,
	stmtUserDetailsSpoilersCount,
	stmtUserDetailsSpoilersCountLast24,
	stmtUserDetailsSpoilsByHour,
	stmtWinner,

	stmtInsertSecretPhrase,
	stmtSecretPhraseByCode *sql.Stmt

var locationET, _ = time.LoadLocation("America/New_York")

// Odds represents the last odds TJ had while drawing.
type Odds struct {
	mutex    *sync.RWMutex
	odds     int
	datetime time.Time
}

// Sweepstakes contains the the state of the current Sweepstakes.
type Sweepstakes struct {
	spoilerMutex         *sync.RWMutex
	spoilerCount         int
	uniqueSpoilerCount   int
	spoilersRemovedCount int

	drawMutex *sync.RWMutex
	winner    bool
	drawCount int

	lastOdds *Odds
}

// Model contains the model logic for the Sweepstakes.
type Model struct {
	DB            *sql.DB
	Sweepstakes   *Sweepstakes
	SecretPhrases *SecretPhrases
}

// SecretPhrases is an object for checking for the secret phrases
type SecretPhrases struct {
	SecretPhrases map[string]bool
	mu            sync.RWMutex
}

// NewSecretPhrases returns a new object
func NewSecretPhrases() *SecretPhrases {
	base64EncodedString := "b2ggYnV0dGVyLiBob3cgbG92ZWx5Lg=="
	decodedPhrase, _ := base64.StdEncoding.DecodeString(base64EncodedString)

	return &SecretPhrases{
		SecretPhrases: map[string]bool{
			"yous guys":           true,
			"tracy klima":         true,
			string(decodedPhrase): true,
		},
	}
}

// IsSecretPhrase returns true if the secret phrase is correct.
func (s *SecretPhrases) IsSecretPhrase(sp string) bool {
	_, ok := s.SecretPhrases[sp]
	return ok
}

// SubmitSecretPhrase will attempt to submit the secret pharse. Only the first submission will be accepted.
// True will be returned if the user won, false otherwise.
// This should only be called after IsSecretPhrase() was checked.
func (s *SecretPhrases) SubmitSecretPhrase(uID int64, un, sp string) (bool, error) {
	// sanity check
	if !s.IsSecretPhrase(sp) {
		return false, nil
	}

	s.mu.Lock()
	defer s.mu.Unlock()
	row := stmtSecretPhraseByCode.QueryRow(sp)
	var found bool
	err := row.Scan(&found)
	if err != sql.ErrNoRows {
		if err != nil {
			return false, err
		}

		return false, nil
	}

	_, err = stmtInsertSecretPhrase.Exec(sp, uID, un)
	if err != nil {
		return false, err
	}

	return true, nil
}

// WsLastOdds represents the odds last time TJ drew
type WsLastOdds struct {
	Odds     int    `json:"odds"`
	Datetime string `json:"datetime"`
}

// WsOverview contains data for the initial payload
type WsOverview struct {
	Type                 WsType      `json:"type"`
	Odds                 int         `json:"odds"`
	SpoilerCount         int         `json:"spoilerCount"`
	UniqueSpoilerCount   int         `json:"uniqueSpoilerCount"`
	DrawCount            int         `json:"drawCount"`
	SpoilersRemovedCount int         `json:"spoilersRemovedCount"`
	HasTJWon             bool        `json:"hasTjWon"`
	LastOdds             *WsLastOdds `json:"lastOdds"`
	Spoilers             []*Spoiler  `json:"spoilers"`
	RecentActivity       []*WsUpdate `json:"recentActivity"`
}

func init() {
}

// NewWsOverview returns a new *WsOverview object.
func NewWsOverview(s *Sweepstakes) *WsOverview {
	o := &WsOverview{
		Type:                 WsTypeOverview,
		Odds:                 s.GetOdds(),
		SpoilerCount:         s.SpoilerCount(),
		SpoilersRemovedCount: s.SpoilersRemovedCount(),
		UniqueSpoilerCount:   s.UniqueSpoilerCount(),
		DrawCount:            s.DrawCount(),
		HasTJWon:             s.HasWinner(),
	}

	spoilers, err := s.TopSpoilers(25)
	if err != nil {
		log.Printf("error: could not obtain top spoilers: %s\n", err)
	}
	o.Spoilers = spoilers

	activities, err := s.recentActivity()
	if err != nil {
		log.Printf("error: could not obtain recent activity: %s\n", err)
	}
	o.RecentActivity = activities

	lo, dt := s.LastOdds()
	o.LastOdds = &WsLastOdds{lo, dt.In(locationET).Format(dateFormat)}

	return o
}

// WsDrawUserDetails represents additional user details for Team TJ users.
type WsDrawUserDetails struct {
	Type               WsType        `json:"type,omitempty"`
	UserID             int64         `json:"userID"`
	Username           string        `json:"username"`
	DrawCount          int           `json:"drawCount"`
	DrawCountInLast24  int           `json:"drawCountInLast24"`
	OddsImproversCount int           `json:"oddsImproversCount"`
	Timeline           map[int64]int `json:"timeline"`
}

// NewWsDrawUserDetailsForUserID instantiates a new WsDrawUserDetails object.
func NewWsDrawUserDetailsForUserID(uID int64) (interface{}, error) {
	row := stmtDrawUserNameFromUserID.QueryRow(uID)
	var un string
	if err := row.Scan(&un); err != nil {
		return nil, err
	}

	tl, err := timelineByStmtAndUserID(stmtUserDetailsDrawsByHour, uID)
	if err != nil {
		return nil, err
	}

	return &WsDrawUserDetails{
		Type:               WsTypeDrawUserDetails,
		UserID:             uID,
		Username:           un,
		DrawCount:          countFromStmt(stmtDrawsCount),
		DrawCountInLast24:  countFromStmt(stmtUserDetailsDrawCountLast24, uID),
		OddsImproversCount: countFromStmt(stmtUserDetailsOddsImproversCount, uID),
		Timeline:           tl,
	}, nil
}

// WsTimeline contains a count against a time in epoch
type WsTimeline map[int64]int

// WsUserEntries contains timelines on a per-user basis
type WsUserEntries struct {
	Type      WsType                `json:"type,omitempty"`
	Timelines map[string]WsTimeline `json:"timelines"`
}

// NewWsUserEntries instantiates a new WsUserEntries object
func NewWsUserEntries() (*WsUserEntries, error) {
	rows, err := stmtUserEntriesCounts.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	tl := make(map[string]WsTimeline)

	for rows.Next() {
		var username string
		var created time.Time
		var count int

		if err := rows.Scan(&username, &created, &count); err != nil {
			return nil, err
		}

		utl, ok := tl[username]
		if !ok {
			utl = make(WsTimeline)
		}

		utl[created.Unix()] = count
		tl[username] = utl
	}

	return &WsUserEntries{
		Type:      WsTypeUserEntries,
		Timelines: tl,
	}, nil
}

// WsSpoilUserDetails represents additional user details
type WsSpoilUserDetails struct {
	Type                  WsType     `json:"type,omitempty"`
	UserID                int64      `json:"userID"`
	Username              string     `json:"username"`
	SpoilersCount         int        `json:"spoilersCount"`
	SpoilersCountInLast24 int        `json:"spoilersCountInLast24"`
	MultipliersCount      int        `json:"multipliersCount"`
	Timeline              WsTimeline `json:"timeline"`
}

// NewWsSpoilUserDetailsForUserID instantiates a new WsSpoilUserDetails object.
func NewWsSpoilUserDetailsForUserID(uID int64) (*WsSpoilUserDetails, error) {
	row := stmtSpoilUserNameFromUserID.QueryRow(uID)
	var un string
	if err := row.Scan(&un); err != nil {
		return nil, err
	}

	tl, err := timelineByStmtAndUserID(stmtUserDetailsSpoilsByHour, uID)
	if err != nil {
		return nil, err
	}

	return &WsSpoilUserDetails{
		Type:                  WsTypeSpoilUserDetails,
		UserID:                uID,
		Username:              un,
		SpoilersCount:         countFromStmt(stmtUserDetailsSpoilersCount, uID),
		SpoilersCountInLast24: countFromStmt(stmtUserDetailsSpoilersCountLast24, uID),
		MultipliersCount:      countFromStmt(stmtUserDetailsMultipliersCount, uID),
		Timeline:              tl,
	}, nil
}

// WsUpdate represents an update which can be passed via WebSockets
type WsUpdate struct {
	Type                 WsType     `json:"type,omitempty"`
	Action               ActionType `json:"action"`
	UserID               int64      `json:"userID"`
	Username             string     `json:"username"`
	Odds                 int        `json:"odds"`
	Datetime             string     `json:"datetime"`
	SpoilerCount         int        `json:"spoilerCount,omitempty"`
	UniqueSpoilerCount   int        `json:"uniqueSpoilerCount,omitempty"`
	DrawCount            int        `json:"drawCount,omitempty"`
	HasTJWon             bool       `json:"hasTjWon,omitempty"`
	SpoilMultiplier      int        `json:"spoilMultiplier,omitempty"`
	SpoilersRemovedPerc  float32    `json:"spoilersRemovedPerc,omitempty"`
	SpoilersRemovedCount int        `json:"spoilersRemovedCount,omitempty"`
}

// NewWsUpdate instantiates a new object for passing an update
func NewWsUpdate(action ActionType, odds int, uID int64, uName string, sc, usc, dc int, won bool) *WsUpdate {
	return &WsUpdate{
		Type:               WsTypeUpdate,
		Action:             action,
		Odds:               odds,
		UserID:             uID,
		Username:           uName,
		Datetime:           time.Now().Format(dateFormat),
		SpoilerCount:       sc,
		UniqueSpoilerCount: usc,
		DrawCount:          dc,
		HasTJWon:           won,
	}
}

// New instanties a new Model object.
func New(d *sql.DB) *Model {
	stmtSpoilersCount = mustPrepare(d.Prepare("SELECT SUM(multiplier) FROM spoilers"))
	stmtUniqueSpoilersCount = mustPrepare(d.Prepare("SELECT COUNT(*) FROM spoilers"))
	stmtSpoilerRemoversSum = mustPrepare(d.Prepare("SELECT SUM(removed) FROM spoiler_removers"))
	stmtDraw = mustPrepare(d.Prepare("SELECT * FROM draw($1, $2, $3, $4, $5)"))
	stmtSpoil = mustPrepare(d.Prepare("SELECT * FROM spoil_v3($1, $2, $3, $4, $5)"))
	stmtWinner = mustPrepare(d.Prepare("SELECT true::boolean FROM draws WHERE winner = 't'"))
	stmtDrawsCount = mustPrepare(d.Prepare("SELECT COUNT(*) FROM draws"))
	stmtTopSpoilers = mustPrepare(d.Prepare("SELECT user_id, (array_agg(username))[1], COUNT(*), 100 * COUNT(*) / SUM(COUNT(*)) OVER() FROM spoilers GROUP by user_id ORDER BY 2 DESC LIMIT $1"))
	stmtOddsLastDraw = mustPrepare(d.Prepare("SELECT current_odds, created FROM draws ORDER BY id DESC LIMIT 1"))
	stmtRecentActivity = mustPrepare(d.Prepare(`
WITH d AS (
SELECT 'draw'::text, user_id, username, winner, current_odds, null::real, null::integer, null::integer, created FROM draws ORDER BY id DESC LIMIT 25
),
s AS (
	SELECT 'spoil'::text, user_id, username, null::boolean, current_odds, null::real, null::integer, multiplier, created FROM spoilers ORDER BY id DESC LIMIT 25
),
r AS (
	SELECT 'remove'::text, user_id, username, null::boolean, current_odds, percentage, removed, null::integer, created FROM spoiler_removers ORDER BY id DESC LIMIT 25
)
SELECT * FROM d
UNION
SELECT * FROM s
UNION
SELECT * FROM r
ORDER BY created DESC
LIMIT 25`))
	stmtSpoilerRemover = mustPrepare(d.Prepare("INSERT INTO spoiler_removers (user_id, username, percentage, removed, current_odds) VALUES ($1, $2, $3, $4, $5)"))
	stmtRandomDrawEntries = mustPrepare(d.Prepare("SELECT username, COUNT(*) FROM spoilers GROUP BY username"))
	stmtUserDetailsSpoilersCount = mustPrepare(d.Prepare("SELECT COUNT(*) FROM spoilers WHERE user_id = $1"))
	stmtUserDetailsSpoilersCountLast24 = mustPrepare(d.Prepare("SELECT COUNT(*) FROM spoilers WHERE user_id = $1 AND created > (NOW() AT TIME ZONE 'UTC' - INTERVAL '1 day')"))
	stmtUserDetailsMultipliersCount = mustPrepare(d.Prepare("SELECT COUNT(*) FROM spoilers WHERE user_id = $1 AND multiplier > 1"))
	stmtSpoilUserNameFromUserID = mustPrepare(d.Prepare("SELECT username FROM spoilers WHERE user_id = $1 ORDER BY id DESC LIMIT 1"))
	stmtDrawUserNameFromUserID = mustPrepare(d.Prepare("SELECT username FROM draws WHERE user_id = $1 ORDER BY id DESC LIMIT 1"))
	stmtUserDetailsDrawCountLast24 = mustPrepare(d.Prepare("SELECT COUNT(*) FROM draws WHERE user_id = $1 AND created > (NOW() AT TIME ZONE 'UTC' - INTERVAL '1 day')"))
	stmtUserDetailsOddsImproversCount = mustPrepare(d.Prepare("SELECT COUNT(*) FROM spoiler_removers WHERE user_id = $1"))
	stmtUserDetailsSpoilsByHour = mustPrepare(d.Prepare("SELECT date_trunc('day', created at time zone 'utc' at time zone 'america/new_york'), COUNT(*) FROM spoilers WHERE user_id = $1 GROUP BY 1 ORDER BY 1"))
	stmtUserDetailsDrawsByHour = mustPrepare(d.Prepare("SELECT date_trunc('day', created at time zone 'utc' at time zone 'america/new_york'), COUNT(*) FROM draws WHERE user_id = $1 GROUP BY 1 ORDER BY 1"))
	stmtUserEntriesCounts = mustPrepare(d.Prepare(`SELECT 'The World', date_trunc('day', created at time zone 'utc' at time zone 'america/new_york'), COUNT(*) FROM spoilers GROUP BY 1,2
UNION
SELECT username, date_trunc('day', created at time zone 'utc' at time zone 'america/new_york'), COUNT(*) FROM draws GROUP BY 1,2
ORDER BY 1`))
	stmtSecretPhraseByCode = mustPrepare(d.Prepare(`SELECT 1 FROM secret_phrases WHERE phrase = $1`))
	stmtInsertSecretPhrase = mustPrepare(d.Prepare(`INSERT INTO secret_phrases (phrase, user_id, username) VALUES ($1, $2, $3)`))

	return &Model{
		DB:            d,
		Sweepstakes:   NewSweepstakes(),
		SecretPhrases: NewSecretPhrases(),
	}
}

// NewOdds instanties a new Odds object.
func NewOdds() *Odds {
	var odds int
	var when time.Time

	row := stmtOddsLastDraw.QueryRow()
	if err := row.Scan(&odds, &when); err != nil {
		if err != sql.ErrNoRows {
			log.Fatalf("error: could not find last draw: %s\n", err)
		}
	}

	return &Odds{
		mutex:    &sync.RWMutex{},
		odds:     odds,
		datetime: when.In(locationET),
	}
}

// NewSweepstakes instantiates a new Sweepstakes object.
func NewSweepstakes() *Sweepstakes {
	s := &Sweepstakes{
		spoilerMutex:         &sync.RWMutex{},
		drawMutex:            &sync.RWMutex{},
		spoilerCount:         countFromStmt(stmtSpoilersCount),
		uniqueSpoilerCount:   countFromStmt(stmtUniqueSpoilersCount),
		spoilersRemovedCount: countFromStmt(stmtSpoilerRemoversSum),
		drawCount:            countFromStmt(stmtDrawsCount),
		lastOdds:             NewOdds(),
	}

	s.winner = s.hasWinner()

	return s
}

// Spoiler represents an individual spoiler user.
type Spoiler struct {
	UserID     int64   `json:"userID"`
	Username   string  `json:"username"`
	Count      int     `json:"count"`
	Percentage float32 `json:"percentage"`
}

// TopSpoilers returns a list of top spoilers.
func (s *Sweepstakes) TopSpoilers(n int) ([]*Spoiler, error) {
	rows, err := stmtTopSpoilers.Query(n)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var spoilers []*Spoiler
	for rows.Next() {
		var sp Spoiler
		if err := rows.Scan(&sp.UserID, &sp.Username, &sp.Count, &sp.Percentage); err != nil {
			return nil, err
		}

		spoilers = append(spoilers, &sp)
	}

	return spoilers, nil
}

// hasWinner will return true if a winner was found in the database.
func (s *Sweepstakes) hasWinner() bool {
	row := stmtWinner.QueryRow()
	var winner bool
	if err := row.Scan(&winner); err != nil {
		if err == sql.ErrNoRows {
			return false
		}

		log.Fatalf("error: could not determine if winner exists: %s\n", err)
	}

	return winner
}

// Draw will attempt an InstantWin for the given uID. The return values are:
//   draw succeeded
//   draw was a winner
//   If submitted too early, how much time user must wait
//   an error
func (s *Sweepstakes) Draw(uID int64, u string) (bool, bool, float32, error) {
	s.drawMutex.Lock()
	defer func() {
		s.drawMutex.Unlock()
	}()

	if s.winner {
		return false, false, 0.0, ErrWinnerExists
	}

	odds := s.GetOdds()
	try, err := s.tryWithOdds(uID, "draw", oddsInt(odds))
	if err != nil {
		return false, false, 0.0, err
	}

	s.SetLastOdds(odds)

	row := stmtDraw.QueryRow(uID, u, try.DidWin, odds, drawWaitDuration)
	var ok bool
	var wait float32
	if err := row.Scan(&ok, &wait); err != nil {
		return false, false, 0.0, err
	}

	if ok {
		try.Log()
		s.drawCount++

		if try.DidWin {
			s.winner = true
		}
	}

	return ok, try.DidWin, wait, nil
}

// ImprovedOdds contains data about the improved odds
type ImprovedOdds struct {
	DidImprove   bool
	RemovedPerc  float32
	RemovedCount int
	NewOdds      int
}

// AttemptToImproveOdds will try to improve the odds. This should only be done
// if a Draw() failed to win the game. The return value will be a tuple containing:
func (s *Sweepstakes) AttemptToImproveOdds(uID int64, u string) (*ImprovedOdds, error) {
	try, err := s.tryWithOdds(uID, "remove-big", oddsSpoilerRemoveBig)
	if err != nil {
		return nil, err
	}
	try.Log()

	var perc float32
	if try.DidWin {
		perc = spoilerRemoveBigPercentage
	} else {
		try, err = s.tryWithOdds(uID, "remove-small", oddsSpoilerRemoveSmall)
		if err != nil {
			return nil, err
		}
		try.Log()

		if !try.DidWin {
			return &ImprovedOdds{}, nil
		}

		perc = spoilerRemoveSmallPercentage
	}

	removed := int(float32(s.TrueSpoilerCount()) * perc)
	if removed == 0 {
		log.Printf("NOTICE: An odds improver of %.1f was technically hit, but there were too few spoilers to remove", 100*perc)
		return &ImprovedOdds{}, nil
	}

	s.spoilerMutex.Lock()
	defer func() {
		s.spoilerMutex.Unlock()
	}()

	newOdds := s.getOddsUsingSpoilerCount(s.spoilerCount, s.spoilersRemovedCount+removed)
	if _, err = stmtSpoilerRemover.Exec(uID, u, perc, removed, newOdds); err != nil {
		log.Printf("SEVERE: could not insert a spoiler remover (%.1f, %d): %s\n", perc, removed, err)
		return &ImprovedOdds{}, err
	}

	s.spoilersRemovedCount += removed
	return &ImprovedOdds{
		DidImprove:   true,
		RemovedPerc:  perc,
		RemovedCount: removed,
		NewOdds:      newOdds,
	}, nil
}

// try represents a random attempt.
type try struct {
	DidWin bool
	logMsg string
}

// Log the random attempt. Only call if the draw/spoil was allowed.
func (r *try) Log() {
	tjlog.Println(r.logMsg)
}

// tryWithOdds attempts to do an actual draw. A tuple is returned with the first value
// being if they won or not.
func (s *Sweepstakes) tryWithOdds(uID int64, thing string, odds oddsInt) (*try, error) {
	r, err := randN(int(odds))
	if err != nil {
		return nil, err
	}

	return &try{
		DidWin: r == 0,
		logMsg: fmt.Sprintf("%s\t%d\t%d:1\t%d", thing, uID, int(odds), r),
	}, nil
}

// Spoil increments the spoiler count. Three values are returned.
//   bool - A success status
//   int - Multiplier
//   float32 - If submitted too early, how much time user must wait
//   error - An error message
func (s *Sweepstakes) Spoil(uID int64, u string) (bool, int, float32, error) {
	s.spoilerMutex.Lock()
	defer func() {
		s.spoilerMutex.Unlock()
	}()

	multiplier := 1
	var trySmall *try
	tryBig, err := s.tryWithOdds(uID, "multiplier-big", oddsSpoilMultiplierBig)
	if err != nil {
		return false, 0, 0.0, err
	} else if tryBig.DidWin {
		multiplier = spoilMultiplierBig
	} else {
		trySmall, err = s.tryWithOdds(uID, "multiplier-small", oddsSpoilMultiplierSmall)
		if err != nil {
			return false, 0, 0.0, err
		} else if trySmall.DidWin {
			multiplier = spoilMultiplierSmall
		}
	}

	odds := s.getOddsUsingSpoilerCount(s.spoilerCount+multiplier, s.spoilersRemovedCount)

	var ok bool
	var waitDuration float32
	row := stmtSpoil.QueryRow(uID, u, odds, multiplier, spoilWaitDuration)
	if err := row.Scan(&ok, &waitDuration); err != nil {
		return false, 0, 0.0, err
	}

	if ok {
		s.spoilerCount += multiplier
		s.uniqueSpoilerCount++
		tjlog.Printf("spoil\t%d\t%d:1\t", uID, odds)
		tryBig.Log()
		if trySmall != nil {
			trySmall.Log()
		}
		return true, multiplier, 0.0, nil
	}

	return false, 0, waitDuration, nil
}

// HasWinner returns if a winner has been found yet.
func (s *Sweepstakes) HasWinner() bool {
	s.drawMutex.RLock()
	w := s.winner
	s.drawMutex.RUnlock()

	return w
}

// SpoilerCount returns the number of spoiler entries plus multipliers successfully submitted.
func (s *Sweepstakes) SpoilerCount() int {
	s.spoilerMutex.RLock()
	c := s.spoilerCount
	s.spoilerMutex.RUnlock()

	return c
}

// TrueSpoilerCount returns the number of spoiler entries plus multipliers successfully submitted minus the ones successfully removed.
func (s *Sweepstakes) TrueSpoilerCount() int {
	s.spoilerMutex.RLock()
	c := s.spoilerCount - s.spoilersRemovedCount
	s.spoilerMutex.RUnlock()

	return c
}

// UniqueSpoilerCount returns the number of unique spoiler entries successfully submitted.
func (s *Sweepstakes) UniqueSpoilerCount() int {
	s.spoilerMutex.RLock()
	c := s.uniqueSpoilerCount
	s.spoilerMutex.RUnlock()

	return c
}

// SpoilersRemovedCount returns the number of spoiler entries successfully removed.
func (s *Sweepstakes) SpoilersRemovedCount() int {
	s.spoilerMutex.RLock()
	c := s.spoilersRemovedCount
	s.spoilerMutex.RUnlock()

	return c
}

// GetOdds retrieves the current odds.
func (s *Sweepstakes) GetOdds() int {
	return s.getOddsUsingSpoilerCount(s.SpoilerCount(), s.SpoilersRemovedCount())
}

// getOddsUsingSpoilerCount allows us to return the odds without invoking worrying about a mutex
func (s *Sweepstakes) getOddsUsingSpoilerCount(sc int, src int) int {
	return baseOdds + spoilerMultiplier*(sc-src)
}

// DrawCount retrieves the number of successful (e.g. not necessarily won) draw entries.
func (s *Sweepstakes) DrawCount() int {
	s.drawMutex.RLock()
	c := s.drawCount
	s.drawMutex.RUnlock()

	return c
}

// SetLastOdds sets the last user action for use in the statistics message.
func (s *Sweepstakes) SetLastOdds(odds int) {
	s.lastOdds.mutex.Lock()
	defer func() {
		s.lastOdds.mutex.Unlock()
	}()

	s.lastOdds.odds = odds
	s.lastOdds.datetime = time.Now()
}

// LastOdds returns the last action item for use with the statistics message.
func (s *Sweepstakes) LastOdds() (int, time.Time) {
	s.lastOdds.mutex.RLock()
	o, d := s.lastOdds.odds, s.lastOdds.datetime
	s.lastOdds.mutex.RUnlock()

	if o == 0 {
		return 0, time.Time{}
	}

	return o, d
}

// RandomDrawWinner represents the winner of a random draw
type RandomDrawWinner struct {
	Username string
	Perc     float32
}

// SelectRandomDrawWinner will choose a winner from the random draw
func (m *Model) SelectRandomDrawWinner() (RandomDrawWinner, error) {
	u, err := m.randomDrawUsersAndEntries()
	if err != nil {
		return RandomDrawWinner{}, err
	}

	keys := make([]string, 0, len(u))
	total := 0
	for key, count := range u {
		total += count
		keys = append(keys, key)
	}

	r, err := randN(total)
	if err != nil {
		return RandomDrawWinner{}, err
	}

	sum := 0
	for _, key := range keys {
		sum += u[key]
		if r < sum {
			return RandomDrawWinner{
				Username: key,
				Perc:     100.0 * float32(u[key]) / float32(total),
			}, nil
		}
	}

	return RandomDrawWinner{}, fmt.Errorf("could not select a random draw winner (total = %d, random number = %d)", total, r)
}

func (m *Model) randomDrawUsersAndEntries() (map[string]int, error) {
	rows, err := stmtRandomDrawEntries.Query()
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	u := make(map[string]int)
	for rows.Next() {
		var uname string
		var count int
		if err := rows.Scan(&uname, &count); err != nil {
			return nil, err
		}

		u[uname] = count
	}

	return u, nil
}

func randN(n int) (int, error) {
	r, err := rand.Int(rand.Reader, big.NewInt(int64(n)))
	if err != nil {
		return 00, err
	}

	return int(r.Int64()), nil
}

func (s *Sweepstakes) recentActivity() ([]*WsUpdate, error) {
	rows, err := stmtRecentActivity.Query()
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var updates []*WsUpdate
	for rows.Next() {
		var u WsUpdate

		var winner sql.NullBool
		var odds sql.NullInt64
		var created time.Time
		var removedPerc sql.NullFloat64
		var removedCount sql.NullInt64
		var multiplier sql.NullInt64

		if err := rows.Scan(&u.Action, &u.UserID, &u.Username, &winner, &odds, &removedPerc, &removedCount, &multiplier, &created); err != nil {
			return nil, err
		}

		u.HasTJWon = winner.Bool
		u.Odds = int(odds.Int64)
		u.Datetime = created.In(locationET).Format(dateFormat)
		u.SpoilersRemovedPerc = float32(removedPerc.Float64)
		u.SpoilersRemovedCount = int(removedCount.Int64)
		u.SpoilMultiplier = int(multiplier.Int64)

		updates = append(updates, &u)
	}

	return updates, nil
}

// mustPrepare will ensure that a prepare statement suceeds or raises a fatal error.
func mustPrepare(s *sql.Stmt, err error) *sql.Stmt {
	if err != nil {
		log.Fatal(err)
	}

	return s
}

// countFromStmt will return an integer count from a "SELECT COUNT(*)"-like statement
func countFromStmt(stmt *sql.Stmt, v ...interface{}) int {
	var count sql.NullInt64
	row := stmt.QueryRow(v...)
	if err := row.Scan(&count); err != nil {
		log.Fatalf("error: could not determine count: %s\n", err)
	}

	return int(count.Int64)
}

func timelineByStmtAndUserID(stmt *sql.Stmt, uID int64) (WsTimeline, error) {
	tl := make(WsTimeline)
	rows, err := stmt.Query(uID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var h time.Time
		var c int
		if err := rows.Scan(&h, &c); err != nil {
			return nil, err
		}

		tl[h.Unix()] = c
	}

	return tl, nil
}
