package main

import (
	"database/sql"
	"fmt"
	"os"

	"gitlab.com/weters/tele/teleconfig"
	"gitlab.com/weters/tjvstheworld/model"

	_ "github.com/lib/pq"
)

const tjVsTheWorldBotName = "TJvsWORLDbot"

func main() {
	b := tjVsTheWorldBot()
	d := b.Database

	dbh, err := sql.Open("postgres", fmt.Sprintf("user=%s dbname=%s password=%s sslmode=disable", d.User, d.Database, d.Password))
	if err != nil {
		fatalf("error: could not connect to db: %s\n", err)
	}

	m := model.New(dbh)
	w, err := m.SelectRandomDrawWinner()
	if err != nil {
		fatalf("error: could not select random draw winner: %s\n", err)
		return
	}

	fmt.Printf("%s won with a %.2f%% chance\n", w.Username, w.Perc)
}

func tjVsTheWorldBot() *teleconfig.Bot {
	for _, b := range teleconfig.BotsConfig {
		if b.Name == tjVsTheWorldBotName {
			return b
		}
	}

	fatalf("could not find " + tjVsTheWorldBotName)
	return nil
}

func fatalf(f string, v ...interface{}) {
	fmt.Fprintf(os.Stderr, f, v...)
	os.Exit(1)
}
