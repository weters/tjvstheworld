package tjvstheworld

import (
	"log"
	"net/http"

	"gitlab.com/weters/tjvstheworld/model"

	"github.com/gorilla/websocket"
)

// WsReceiveAction represnts an action
type WsReceiveAction string

// WsReceive is data posted from the WS client to the server
type WsReceive struct {
	Action WsReceiveAction `json:"action"`
	ID     int64           `json:"id,omitempty"`
	Team   model.Team      `json:"team,omitempty"`
}

// Client represents an individual client connection.
type Client struct {
	conn *websocket.Conn
	send chan interface{}
}

// WsReceiveAction are actions in a WsReceive object
const (
	WsReceiveUserDetails WsReceiveAction = "user_details"
	WsReceiveUserEntries                 = "user_entries"
)

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

var hub *Hub

func newClient(conn *websocket.Conn) *Client {
	return &Client{
		conn: conn,
		send: make(chan interface{}, 256),
	}
}

// Hub represents the object responsible for managing client connections and sending messages to those clients.
type Hub struct {
	clients    map[*Client]bool
	broadcast  chan interface{}
	register   chan *Client
	unregister chan *Client
}

// SetupWebSockets will setup the Hub. This should be called prior to setting up the web handlers.
func SetupWebSockets() error {
	hub = newHub()
	go hub.run()

	return nil
}

// SendData will broadcast data to all connected clients.
func SendData(data interface{}) {
	if hub == nil {
		log.Printf("error: hub is not setup")
		return
	}

	hub.broadcast <- data
}

// ServeWS will upgrade a request to a WebSocket connection and setup read and write pumps
func ServeWS(w http.ResponseWriter, r *http.Request) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		log.Printf("error: could not setup websocket: %s\n", err)
		return
	}

	client := newClient(conn)
	hub.register <- client

	go client.writePump()
	client.readPump()
}

func newHub() *Hub {
	return &Hub{
		clients:    make(map[*Client]bool),
		broadcast:  make(chan interface{}),
		register:   make(chan *Client),
		unregister: make(chan *Client),
	}
}

func (h *Hub) run() {
	for {
		select {
		case client := <-h.register:
			log.Printf("registering a client: %s", client.conn.RemoteAddr().String())
			h.clients[client] = true

			client.send <- model.NewWsOverview(c.Model.Sweepstakes)
		case client := <-h.unregister:
			log.Printf("unregistering client: %s", client.conn.RemoteAddr().String())
			delete(h.clients, client)
			close(client.send)
		case msg := <-h.broadcast:
			for client := range h.clients {
				select {
				case client.send <- msg:
				default:
					close(client.send)
					delete(h.clients, client)
				}
			}
		}
	}
}

func (c *Client) writePump() {
	defer func() {
		c.conn.Close()
	}()

	for {
		msg, ok := <-c.send
		if !ok {
			c.conn.WriteMessage(websocket.CloseMessage, []byte{})
			return
		}

		if err := c.conn.WriteJSON(msg); err != nil {
			log.Printf("error: could not write JSON: %s\n", err)
			return
		}
	}
}

func (client *Client) readPump() {
	defer func() {
		hub.unregister <- client
		client.conn.Close()
	}()

	for {
		var r WsReceive
		if err := client.conn.ReadJSON(&r); err != nil {
			log.Printf("error: could not read JSON from WebSocket: %s\n", err)
			client.conn.Close()
			return
		}

		switch r.Action {
		case WsReceiveUserDetails:
			c.sendUserDetailsToClient(r.ID, client)
		case WsReceiveUserEntries:
			c.sendUserEntriesToClient(client)
		}
	}
}
