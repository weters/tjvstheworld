// Package tjvstheworld is a Telegram Bot for running the TJ vs. The World Sweepstakes.
package tjvstheworld

import (
	"database/sql"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	yaml "gopkg.in/yaml.v2"

	"gitlab.com/weters/tjvstheworld/model"
	"gitlab.com/weters/tjvstheworld/tjlog"

	"github.com/weters/telegram/bot"
)

// BotName is the name of the TJ vs. The World Bot.
const BotName = "TJvsWORLDbot"

const defaultConfigFile = "./config.yml"
const defaultLogFile = "/home/tom/go/runners/logs/tjvstheworld.log"

var et = mustLoadLocation(time.LoadLocation("America/New_York"))

// PeriodStart is start time for the sweepstakes.
var PeriodStart = time.Date(2016, 12, 27, 12, 0, 0, 0, et)

// PeriodEnd is end time for the sweepstakes.
var PeriodEnd = time.Date(2017, 1, 27, 23, 59, 59, 59, et)

// DrawPeriodStart is the delay after PeriodStart on when the Instant Win portion begins.
var DrawPeriodStart = PeriodStart.Add(time.Hour)

// ChatID is the Telegram Chat ID of the Discusion thread.
var ChatID int64

// TargetID is the ID of the TJ user.
var TargetID int64

var c *Controller

type config struct {
	UserIDs  map[int64]bool `yaml:"user_ids"`
	TargetID int64          `yaml:"target_id"`
	ChatID   int64          `yaml:"chat_id"`
}

var conf config

func init() {
	configFile := os.Getenv("CONFIG_FILE")
	if configFile == "" {
		configFile = defaultConfigFile
	}

	b, err := ioutil.ReadFile(configFile)
	if err != nil {
		log.Fatalf("could not open %s: %s\n", configFile, err)
	}

	if err := yaml.Unmarshal(b, &conf); err != nil {
		log.Fatalf("could not unmarshal %s: %s\n", configFile, err)
	}

	if conf.TargetID == 0 {
		log.Fatalf("config does not have target_id defined")
	}

	if conf.ChatID == 0 {
		log.Fatalf("config does not have chat_id defined")
	}

	if len(conf.UserIDs) == 0 {
		log.Fatalf("config does not have user_ids defined")
	}

	TargetID = conf.TargetID
	ChatID = conf.ChatID
}

// HandleFunc is a function which returns an http.HandleFunc. This function will be called by
// gitlab.com/weters/tele.
func HandleFunc(d *sql.DB, token string) http.HandlerFunc {
	logFile := defaultLogFile
	if lf := os.Getenv("DRAW_LOGFILE"); lf != "" {
		logFile = lf
	}
	if err := tjlog.Setup(logFile); err != nil {
		log.Fatalf("error: could not setup tjlog: %s\n", err)
	}

	b := bot.New(BotName, token)
	b.Debug = true

	c = NewController(model.New(d))

	b.AddCommandHandler("statistics", c.StatisticsHandler)
	b.AddCommandHandler("help", c.HelpHandler)
	b.AddCommandHandler("rules", c.RulesHandler)
	b.AddCommandHandler("practicedraw", c.PracticeDrawHandler)
	b.SetDefaultHandler(c.DefaultHandler)

	return func(w http.ResponseWriter, r *http.Request) {
		err := b.HandleUpdate(r)
		if err != nil {
			log.Printf("error: handle update had error: %s\n", err)
		}

		w.Header().Set("Content-Type", "text/plain")
		w.WriteHeader(http.StatusOK)
	}
}

func mustLoadLocation(l *time.Location, err error) *time.Location {
	if err != nil {
		log.Fatalf("error: could not load location: %s\n", err)
	}

	return l
}
